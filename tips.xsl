﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
  <html>
  <head>
  <link rel="stylesheet" href="style.css" type="text/css"/>
  </head>
  <body>
  <h2><xsl:value-of select="Race/Title"/></h2>
  <table border="0" CELLSPACING="8">
  <xsl:for-each select="Race/Horse">
  <tr><td><h3><xsl:value-of select="."/></h3></td></tr>
  </xsl:for-each>
  </table>
  <h2>投注介紹</h2>
  <h3><xsl:value-of select="Race/Remark"/></h3>
  </body>
  </html>
</xsl:template>
</xsl:stylesheet>